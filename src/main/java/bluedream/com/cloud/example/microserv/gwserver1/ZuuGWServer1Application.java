package bluedream.com.cloud.example.microserv.gwserver1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableEurekaClient    // It acts as a eureka client
@EnableZuulProxy        // Enable Zuul
public class ZuuGWServer1Application {

	public static void main(String[] args) {
		SpringApplication.run(ZuuGWServer1Application.class, args);
	}

}
